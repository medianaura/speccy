module.exports = {
  overrides: [],
  rules: {
    '@typescript-eslint/class-literal-property-style': 'off',
    '@typescript-eslint/comma-dangle': 'off',
    '@typescript-eslint/consistent-indexed-object-style': 'warn',
    '@typescript-eslint/indent': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/quotes': 'off',
    'arrow-parens': 'off',
    curly: 'off',
    'import/no-unassigned-import': 'off',
    indent: 'off',
    'new-cap': 'off',
    'object-curly-spacing': 'off',
    'unicorn/no-array-for-each': 'off',
    'unicorn/no-array-reduce': 'warn',
  },
};
