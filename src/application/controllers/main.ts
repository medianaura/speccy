import walkBack from 'walk-back';
import ignore from 'ignore';
import endent from 'endent';
import path from 'path';
import fs from 'fs';
import fsExtra from 'fs-extra';
import globby from 'globby';
import { ParsedArgumentsObject } from '@caporal/core';
import { sprintf } from 'sprintf-js';
import chalk from 'chalk';

export class Main {
  private readonly template = endent`describe('NAME', () => {
  it('sample spec.', () => {
    // TODO : It's RED... Make it Green and Refactor !
    expect(false).toBe(true);
  });
});
`;

  public start(args: ParsedArgumentsObject): number {
    const ig = ignore();

    const srcDir = walkBack(process.cwd(), 'src');
    const rootDir = path.resolve(srcDir, '..');
    const specDir = path.resolve(rootDir, 'tests/unit');

    process.chdir(rootDir);

    if (fs.existsSync('./.speccyignore')) {
      ig.add(fs.readFileSync('./.speccyignore').toString());
    }

    const glob = args.glob.toString().replace(/\\/gm, '/');
    const files = globby.sync(glob, { cwd: rootDir }).map((file) => path.relative(rootDir, file));

    // eslint-disable-next-line unicorn/no-array-callback-reference
    ig.filter(files).forEach((file) => {
      const specFile = path.resolve(specDir, file).replace(path.extname(file), '.spec.ts');
      if (fs.existsSync(specFile)) {
        console.log(sprintf('Le fichier <%s> est déja présent sur disque.', chalk.redBright(specFile)));
        return;
      }

      fsExtra.ensureDirSync(path.dirname(specFile));
      console.log(sprintf('Le fichier <%s> a été créer avec %s.', chalk.cyanBright(specFile), chalk.greenBright('succès')));
      fs.writeFileSync(specFile, this.template, { encoding: 'utf8' });
    });

    return 0;
  }
}
