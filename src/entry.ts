import 'reflect-metadata';
import 'core-js';

import { program } from '@caporal/core';
import packages from '../package.json';
import { Main } from '@src/application/controllers/main';

program
  .name(packages.name)
  .version(packages.version)
  .strict(false)
  .argument('<glob>', 'Glob vers les fichiers a généré', { default: './src/**/*.{vue,js,ts}' })
  .action(async ({ args }) => {
    return new Main().start(args);
  });

(async () => {
  await program.run();
})();
