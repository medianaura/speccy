import { Expose, Transform } from 'class-transformer';
import { convertMapToPlain } from '@common/helpers/utility';

export class LocaleInformation {
  @Expose()
  public default = 'fr';

  @Expose()
  @Transform(({ value }) => new Map(value ? Object.entries(value) : Object.entries({})), { toClassOnly: true })
  @Transform(({ value }) => convertMapToPlain(value), { toPlainOnly: true })
  public output!: Map<string, string>;
}
