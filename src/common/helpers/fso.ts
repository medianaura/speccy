import { readFileSync as read, outputFileSync } from 'fs-extra';

const iconvlite = require('iconv-lite');

export function readFileSync(nomFichier: string, encoding: string): string {
  let sContent = read(nomFichier);
  sContent = iconvlite.decode(sContent, encoding);
  return sContent.toString();
}

export function writeFileSync(nomFichier: string, content: string, encoding: string): void {
  const buffer = iconvlite.encode(content, encoding);
  outputFileSync(nomFichier, buffer);
}
