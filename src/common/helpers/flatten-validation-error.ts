import { ValidationError } from 'class-validator';

export function flattenValidationError(errors: ValidationError[], property: string[]): ValidationError[] {
  const list: ValidationError[] = [];

  errors.forEach((currentValue) => {
    property.push(currentValue.property);

    if (currentValue.constraints) {
      currentValue.property = property.join('.');
      list.push(currentValue);
    }

    if (currentValue.children) {
      list.push(...flattenValidationError(currentValue.children, property));
    }

    property.pop();
  });

  return list;
}
