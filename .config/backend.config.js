const path = require('path');
const webpack = require('webpack');
const config = require('./default.config');

const c = {
  entry: {
    main: path.resolve(__dirname, '../src/entry.ts'),
  },
  output: {
    path: path.resolve(__dirname, '../bin'),
    filename: '[name].js',
  },
};

module.exports = (env, argv) => {
  Object.assign(config, c);

  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(argv.mode === 'production' ? 'PROD' : 'DEV'),
        BASE_URL: '"./"',
      },
    }),
  );

  config.plugins.push(new webpack.BannerPlugin({ banner: '#!/usr/bin/env node', raw: true }));

  config.optimization.minimize = argv.mode === 'production';

  return config;
};
